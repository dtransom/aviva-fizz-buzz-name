# README #

* An asp.net MVC application to implement the FizzBuzz problem

**Done**

1. Basic Fizz-Buzz:
* The user should be able to enter a positive integer
* You should print out, in a vertical list, all of the values between 1 and the entered value
* Where the number is divisible by 3, you should instead print ‘fizz’
* Where the number is divisible by 5, you should instead print 'buzz'
* Where the number is divisible by 3 AND 5, you should instead print ‘fizz buzz’

2. Add validation to ensure the entered value is an integer between 1 and 1000
3. Add styling so 'fizz' is printed in blue, and 'buzz' in green
4. Change the logic so that if today is Wednesday, the words 'wizz' and ‘wuzz' are substituted for 'fizz' and 'buzz

**Still to be done **

* Amend the program so only 20 values are displayed at a time. Implement ‘Next’ and ‘Previous’ buttons to display the remaining values.

* A RESTful web service is required  to record the user entered values.
* Design and document this web service
* Implement it as a mock or stub in your code.
  
