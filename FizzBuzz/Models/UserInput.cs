﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Models
{
    public class UserInput
    {
        [Range(1, 1000)]
        [Display(Name = "Number")]
        public int UserEntry { get; set; }
    }
}