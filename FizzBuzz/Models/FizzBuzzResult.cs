﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Models
{
    public class FizzBuzzResult
    {
        [Display(Name = "Results from your entry")]
        public string FizzBuzzRes { get; set; }

        public string FontColor { get; set; }
    }
}