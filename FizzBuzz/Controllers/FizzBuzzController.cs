﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Models;

namespace FizzBuzz.Controllers
{
    public class FizzBuzzController : Controller
    {
  
        public ActionResult Index([Bind(Include = "UserEntry")] UserInput userInput)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Results", new { input = userInput.UserEntry});
            }

            return View(userInput);
        }

        public ActionResult Results(int? input)
        {
            if (input != null)
            {
                var fizzBuzzResultAsString = GetResultsWithFizzBuzz((int) input, DateTime.Now.DayOfWeek);
                var fizzBuzzResults = new List<FizzBuzzResult>();
                foreach (var returnedString in fizzBuzzResultAsString)
                {
                    var fb = new FizzBuzzResult {FizzBuzzRes = returnedString};

                    switch (returnedString)
                    {
                        case "Fizz":
                            fb.FontColor = "Blue";
                            break;
                        case "Buzz":
                            fb.FontColor = "Green";
                            break;
                        default:
                            fb.FontColor = "Black";
                            break;
                    }

                    fizzBuzzResults.Add(fb);

                }

                return View(fizzBuzzResults);
            }
            return RedirectToAction("Index");
        }

        public List<string> GetResultsWithFizzBuzz(int userEntry, DayOfWeek dayOfWeek)
        { 
            var fizzBuzzList = new List<string>();

            for (var i = 1; i <= userEntry; i++)
            {
                fizzBuzzList.Add(GetNextFizzBuzzResult(i, dayOfWeek));
            }
            return fizzBuzzList;
        }

        private string GetNextFizzBuzzResult(int position, DayOfWeek dayOfWeek)
        {
            var randomWord1 = "Fizz";
            var randomWord2 = "Buzz";

            if (dayOfWeek == DayOfWeek.Wednesday)
            {
                randomWord1 = "Wizz";
                randomWord2 = "Wuzz";
            }


            if (position % 15 == 0)
            {
                return (randomWord1 + " " + randomWord2);
            }
            if (position % 3 == 0)
            {
                return (randomWord1);
            }
            if (position % 5 == 0)
            {
                return (randomWord2);
            }
            return position.ToString();
        }



    }
}