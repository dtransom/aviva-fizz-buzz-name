﻿using System;
using System.Collections.Generic;
using System.Linq;
using FizzBuzz.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace FizzBuzz.Tests.Controllers
{
    [TestClass]
    public class FizzBuzzControllerTest
    {

        [TestMethod]
        public void TestFizzBuzz_ShouldReturnTwoItems()
        {
            //Arrange
            var fizzBuzzController = new FizzBuzzController();
            var expected = new List<string> {"1", "2"};
            var dayOfWeek = DateTime.Now.DayOfWeek; 

            //Act
            var actual = fizzBuzzController.GetResultsWithFizzBuzz(2, DayOfWeek.Monday);

            //Assert
            CollectionAssert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestFizzBuzz_ShouldReturnThreeItems()
        {
            //Arrange
            var fizzBuzzController = new FizzBuzzController();
            var expected = new List<string> {"1", "2", "Fizz"};

            //Act
            var actual = fizzBuzzController.GetResultsWithFizzBuzz(3, DayOfWeek.Monday);

            //Assert
            CollectionAssert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void TestFizzBuzz_ShouldReturnFiveItems()
        {
            //Arrange
            var fizzBuzzController = new FizzBuzzController();
            var expected = new List<string> {"1", "2", "Fizz", "4", "Buzz"};

            //Act
            var actual = fizzBuzzController.GetResultsWithFizzBuzz(5, DayOfWeek.Monday);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestFizzBuzz_The15thWordInTestListShouldBeFizzBuzz()
        {
            //Arrange
            var fizzBuzzController = new FizzBuzzController();
           
            var position = 15;  
            var expected = "Fizz Buzz";

            //Act
            var actual = fizzBuzzController.GetResultsWithFizzBuzz(position, DayOfWeek.Monday);
            var resultAtPostion15OfTheList = actual.ElementAt(position - 1); // Zero Relative

            // Assert
            Assert.AreEqual(expected, resultAtPostion15OfTheList);
        }

        [TestMethod]
        public void TestFizzBuzz_ShouldReturnFiveItemsWithWizzWuzzOnWednesday()
        {
            //Arrange
            var fizzBuzzController = new FizzBuzzController();
            var expected = new List<string> { "1", "2", "Wizz", "4", "Wuzz" };
            var dayOfWeek = DayOfWeek.Wednesday;

            //Act
            var actual = fizzBuzzController.GetResultsWithFizzBuzz(5, dayOfWeek);

            //Assert
            CollectionAssert.AreEqual(expected, actual);
        }

    }
}
